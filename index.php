<?php
// echo '<pre>';
// print_r($GLOBALS);
// print_r($_SERVER);
// print_r($_GET);
// print_r($_POST);
// print_r($_FILES);
// print_r($_REQUEST);
// print_r($_SESSION);
// print_r($_ENV);
// print_r($_COOKIE);
// print_r($php_errormsg);
// print_r($HTTP_RAW_POST_DATA);
// print_r($http_response_header);
// print_r($argc);
// print_r($argv);
// echo '</pre>';
// die();

// Limpieza PowerShell   Get-ChildItem -recurse | ? {$_.Name -like “*(1)*”} | Remove-Item -Recurse -Force

// [REQUEST_URI] => /siscoed/contenidos/Sample/Recorte/body2
// [SCRIPT_NAME] => /siscoed/index.php

/**
 * A simple, clean and secure PHP Login Script
 * 
 * MVC FRAMEWORK VERSION
 * Check Github for other versions
 * Check develop branch on Github for bleeding edge versions
 * 
 * A simple PHP Login Script embedded into a small framework.
 * Uses PHP sessions, the most modern password-hashing and salting
 * and gives all major functions a proper login system needs.
 * 
 * @package php-mvc
 * @author Panique <panique@web.de>
 * @link http://www.php-login.net
 * @link https://github.com/panique/php-login/
 * @license http://opensource.org/licenses/MIT MIT License
 */

ini_set('upload_max_filesize', '64M');
ini_set('post_max_size', '64M');


function str_replace_first($from, $to, $subject) {
    $from = '/'.preg_quote($from, '/').'/';
    return preg_replace($from, $to, $subject, 1);
}

/**
 * Configuration for: Base URL
 * This is the base url of our app. if you go live with your app, put your full domain name here.
 * if you are using a (differen) port, then put this in here, like http://mydomain:8888/mvc/
 * TODO: when not using subfolder, is the trailing slash important ?
 */
//define('URL', 'http://127.0.0.1/php-login/4-full-mvc-framework/');
//define('URL', 'http://127.0.0.1/php-login/php-login/');
if(dirname($_SERVER['SCRIPT_NAME']) == '/') {
	define('URL', '/');
} else {
	define('URL', dirname($_SERVER['SCRIPT_NAME']).'/');
}
if (!isset($_GET['url'])) {
	// $_GET['url'] = str_replace(URL, '', $_SERVER['REQUEST_URI']);
	$_GET['url'] = str_replace_first(URL, '', $_SERVER['REQUEST_URI']);
}

if(dirname($_SERVER['SCRIPT_FILENAME']) == '/') {
	define('ROOT_PATH', '/');
} else {
	define('ROOT_PATH',dirname($_SERVER['SCRIPT_FILENAME']).'/');
}
session_set_cookie_params(0, URL);

define('FRNT_URL',URL);
define('FRNT_ROOT_PATH',ROOT_PATH);

define('LIB_PATH',FRNT_ROOT_PATH.'libs/');
define('MODEL_PATH',FRNT_ROOT_PATH.'models/');
define('CFG_PATH',FRNT_ROOT_PATH.'config/');
// define('ASSETS_PATH',FRNT_ROOT_PATH.'assets/');

// dev error reporting
error_reporting(E_ALL);
ini_set("display_errors", 1);

// checking for minimum PHP version
if (version_compare(PHP_VERSION, '5.3.7', '<') ) {
  exit("Sorry, Simple PHP Login does not run on a PHP version smaller than 5.3.7 !");  
}

//$tmp = file_get_contents('http://mdservidor.info/scalcograf/validacion.php');

// echo IMG_SUPERIOR_WIDTH;

// the autoloading function, which will be called every time a file "is missing"
// NOTE: don't get confused, this is not "__autoload", the now deprecated function
// The PHP Framework Interoperability Group (@see https://github.com/php-fig/fig-standards) recommends using a
// standardized autoloader https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md, so we do:
function autoload($class) {
	// echo $class.'<br>';
	if (file_exists(LIB_PATH . $class . ".php")) { require_once LIB_PATH . $class . ".php"; }
	elseif (file_exists(LIB_PATH . strtolower($class) . ".php")) { require_once LIB_PATH . strtolower($class) . ".php"; }
	elseif (file_exists(MODEL_PATH . $class . ".php")) { require_once MODEL_PATH . $class . ".php"; }
	elseif (file_exists(MODEL_PATH . strtolower($class) . "_model.php")) { require_once MODEL_PATH . strtolower($class) . "_model.php"; }
	elseif (file_exists(LIB_PATH . str_replace('_','/',$class) . ".php")) { require_once LIB_PATH . str_replace('_','/',$class) . ".php"; }
	else{
		// echo 'This-'.$class.'<br>';
	}
}

function exception_handler($exception) {
	echo '<pre>';
	echo "Uncaught exception: " , $exception->getMessage(), "\n";
	// die();
	echo "Error in page. Please consult with administrator.";
	echo '</pre>';
}

set_exception_handler('exception_handler');

// spl_autoload_register defines the function that is called every time a file is missing. as we created this
// function above, every time a file is needed, autoload(THENEEDEDCLASS) is called
spl_autoload_register("autoload");

// loading config
require CFG_PATH.'config.php';
require CFG_PATH.'constants.php';
require CFG_PATH.'global_lists.php';

setlocale(LC_ALL,"es_CO","es_CO","esp");
// $date = DateTime::createFromFormat("d/m/Y", $string);

// stream_context_set_default(['http'=>['proxy'=>'http://lacides.miranda@Felipe0808:colbtawebproxy01:8080']]);

// die();

// $trans = new Translate();
// start our app
$app = new Bootstrap();

/** /
echo "<pre>";
print_r($_GET);
echo "</pre>";

/**/

