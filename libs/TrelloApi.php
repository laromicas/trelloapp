<?php

class TrelloApi
{
    protected $apiVersion = 1;

    protected $apiEndpoint = 'https://api.trello.com';

    protected $authEndpoint = 'https://trello.com';

    protected $baseUrl = 'https://api.trello.com/1/';

    protected $key;

    protected $shared_secret;

    protected $token;

    protected $oauth_secret;

    protected $lastError;

    public function __construct($key, $token = null)
    {
        $this->baseUrl = "$this->apiEndpoint/$this->apiVersion/";
        $this->key = $key;
        $this->shared_secret = null;
        $this->token = $token;
        $this->oauth_secret = null;
    }

    public function version()
    {
        return $this->apiVersion;
    }

    public function key()
    {
        return $this->key;
    }

    public function setKey($key)
    {
        $this->key = $key;
    }

    public function token()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function oauthSecret() {
        return $this->oauth_secret;
    }

    public function setOauthSecret($secret) {
        $this->oauth_secret = $secret;
    }

    public function authorized()
    {
        return $this->token != null;
    }

    public function error()
    {
        return $this->lastError;
    }

    public function authorize($userOptions = array(), $return = false)
    {
        if ($this->authorized()) {
            return true;
        }

        if (!$this->shared_secret) {
            return false;
        }

        $oauth = new OAuthSimple($this->key, $this->shared_secret);

        if (isset($_GET['oauth_verifier'], $_SESSION['oauth_token_secret'])) {
            $signatures = array(
                'oauth_secret' => $_SESSION['oauth_token_secret'],
                'oauth_token' => $_GET['oauth_token'],
            );

            $request = $oauth->sign(array(
                'path' => "$this->authEndpoint/$this->apiVersion/OAuthGetAccessToken",
                'parameters' => array(
                    'oauth_verifier' => $_GET['oauth_verifier'],
                    'oauth_token' => $_GET['oauth_token'],
                ),
                'signatures' => $signatures,
            ));

            $ch = curl_init($request['signed_url']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);

            parse_str($result, $returned_items);
            $this->token = $returned_items['oauth_token'];
            $this->oauth_secret = $returned_items['oauth_token_secret'];

            unset($_SESSION['oauth_token_secret']);

            return true;
        }

        $options = array_merge(array(
            'name' => null,
            'redirect_uri' => $this->callbackUri(),
            'expiration' => '30days',
            'scope' => array(
                'read' => true,
                'write' => false,
                'account' => false,
            ),
        ), $userOptions);

        $scope = implode(',', array_keys(array_filter($options['scope'])));

        $request = $oauth->sign(array(
            'path' => "$this->authEndpoint/$this->apiVersion/OAuthGetRequestToken",
            'parameters' => array(
                'oauth_callback' => $options['redirect_uri'],
            )
        ));

        $ch = curl_init($request['signed_url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        parse_str($result, $returned_items);
        $request_token = $returned_items['oauth_token'];
        $_SESSION['oauth_token_secret'] = $returned_items['oauth_token_secret'];

        $request = $oauth->sign(array(
            'path' => "$this->authEndpoint/$this->apiVersion/OAuthAuthorizeToken",
            'parameters' => array(
                'oauth_token' => $request_token,
                'name' => $options['name'],
                'expiration' => $options['expiration'],
                'scope' => $scope,
            )
        ));

        if ($return) {
            return $request['signed_url'];
        }

        header("Location: $request[signed_url]");
        exit;
    }

    public function __call($method, $arguments)
    {
        if (in_array($method, array('get', 'post', 'put', 'del'))) {
            array_unshift($arguments, strtoupper($method));
            return call_user_func_array(array($this, 'rest'), $arguments);
        }

        throw new \Exception("Method $method does not exist.");
    }

    public function __get($collection)
    {
        return new Collection($collection, $this);
    }

    public function rest($method)
    {
        $args = array_slice(func_get_args(), 1);
        extract($this->parseRestArgs($args)); /* path, params */

        $restData = array();
        if ($this->key && !$this->shared_secret) {
            $restData['key'] = $this->key;
        }
        if ($this->token && !$this->shared_secret) {
            $restData['token'] = $this->token;
        }

        if (is_array($params)) {
            $restData = array_merge($restData, $params);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, "php/1");
        curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
        curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);

        switch ($method) {
            case 'GET':
                break;
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($restData, '', '&'));
                $restData = array();
                break;
            case 'PUT':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($restData, '', '&'));
                $restData = array();
                break;
            case 'DEL':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                break;
            default:
                throw new \Exception('Invalid method specified');
                break;
        }

        $url = $this->buildRequestUrl($method, $path, $restData);
        // var_dump($url);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $responseBody = curl_exec($ch);
        if (!$responseBody) {

            $this->lastError = curl_error($ch);
            var_dump($this->lastError);
            return false;
        }

        $responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $responseBody = trim($responseBody);
        if (substr($responseCode, 0, 1) != '2') {

            $this->lastError = $responseBody;
            return false;
        }

        $this->lastError = '';
        return json_decode($responseBody);
    }

    public function buildRequestUrl($method, $path, $data)
    {
        $url = "{$this->baseUrl}{$path}";

        if ($this->canOauth()) {
            $oauth = new OAuthSimple($this->key, $this->shared_secret);
            $oauth->setTokensAndSecrets(array('access_token' => $this->token,'access_secret' => $this->oauth_secret,))
                  ->setParameters($data);
            $request = $oauth->sign(array('path' => $url));
            return $request['signed_url'];
        } else {
            if (in_array($method, array('GET', 'DELETE', 'DEL')) && !empty($data)) {
                $url .= '?' . http_build_query($data, '', '&');
            }
            return $url;
        }
    }

    protected function parseRestArgs($args)
    {
        $opts = array(
            'path' => '',
            'params' => array(),
        );

        if (!empty($args[0])) {
            $opts['path'] = $args[0];
        }
        if (!empty($args[1])) {
            $opts['params'] = $args[1];
        }

        return $opts;
    }

    protected function canOauth()
    {
        return $this->key && $this->token && $this->shared_secret && $this->oauth_secret;
    }

    protected function callbackUri() {
        if (empty($_SERVER['REQUEST_URI'])) {
            return '';
        }
        $port = $_SERVER['SERVER_PORT'] == '80' || $_SERVER['SERVER_PORT'] == '443' ? '' : ":$_SERVER[SERVER_PORT]";
        $protocol = 'http' . (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) != 'off' ? 's://' : '://');
        return "{$protocol}{$_SERVER['HTTP_HOST']}{$port}{$_SERVER['REQUEST_URI']}";
    }
}

class Collection
{
    protected $collection;
    protected $trello;
    protected $collections = array(
        'actions',
        'boards',
        'cards',
        'checklists',
        'lists',
        'members',
        'notifications',
        'organizations',
        'search',
        'tokens',
        'types',
        'webhooks',
    );

    public function __construct($collection, $trello)
    {
        if (!in_array($collection, $this->collections)) {
            throw new \Exception("Unsupported collection: {$collection}.");
        }

        $this->collection = $collection;
        $this->trello = $trello;
    }

    public function __call($method, $arguments)
    {
        if (empty($arguments)) {
            throw new \Exception('Missing path from method call.');
        }
        $path = array_shift($arguments);
        array_unshift($arguments, "$this->collection/$path");
        return call_user_func_array(array($this->trello, $method), $arguments);
    }
}
