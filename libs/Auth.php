<?php

class Auth {
	public static function handleLogin() {
		Session::init();
		if (Session::get('user_logged_in') == false) {
			Session::set('URI',$_SERVER['REQUEST_URI']);
			Html::redirect_to(URL.'login/index');
			exit();
		}
		return Session::get("user");
	}
}