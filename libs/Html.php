<?php

class Html {


	public static function protocol() {
		if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
			$protocol = 'https://';
		} else {
			$protocol = 'http://';
		}
		return $protocol;
	}

	public static function redirect_to($url) {
		header('location: '.$url);
		die();
	}
}