<?php

class View {

	/**
	 * simply includes (=shows) the view. this is done from the controller. In the controller, you usually say
	 * $this->view->render('help/index'); to show (in this example) the view index.php in the folder help.
	 * Usually the Class and the method are the same like the view, but sometimes you need to show different views.
	 * @param string $filename Path of the to-be-rendered view, usually folder/file(.php)
	 * @param boolean $special_page Optional: Set this to true if you don't want to include header and footer
	 */

	public $externalJavascripts = array();
	public $AngularJs = array();
	public $PluginJs = array();
	public $externalStyles = array();
	public $Javascripts = array();
	public $Styles = array();
	public $sideMenu = array();

	function __construct() {
		
	}


	public function render($filename, $params = array()) {
		$js = $filename;
		$css = $filename;
		$path = 'views/';
		$headerfooter = true;
		$header = 'header';
		$footer = 'footer';
		$ext = 'php';
		$loadSideMenu = 'true';
		// extract($params,EXTR_IF_EXISTS);
		foreach ($params as $key => $value) {
			$$key = $value;
		}
		$ext = ($ext) ? '.'.$ext : '';

		if(file_exists('assets/js/views/' . $js . '.js')) {
			$this->addJavascript('views/' . $js );
		}
		if(file_exists('assets/css/views/' . $css . '.css')) {
			$this->addStyle('views/' . $css );
		}


		$headerpath = (@$headerpath) ? $headerpath : $path;
		$footerpath = (@$footerpath) ? $footerpath : ((@$headerpath) ? $headerpath : $path);
		$headerext = (@$headerext) ? '.' . $headerext : $ext;
		$footerext = (@$footerext) ? '.' . $footerext : ((@$headerext) ? $headerext : $ext);

		if ($headerfooter && $loadSideMenu) {
			// $this->sideMenu = json_decode(file_get_contents(CFG_PATH.'sidemenu.json'),true);
			require CFG_PATH.'sidemenu.php';
			$this->sideMenu = $GLOBALS["sidemenu"];
		}
		if ($headerfooter && $header) {
			require $headerpath . $header . $headerext;
		}
		require $path . $filename . $ext;
		if(!$headerfooter && file_exists('assets/js/views/' . $js . '.js')) {
			echo '<script language="javascript">'."\xA";
			echo file_get_contents('assets/js/views/' . $js . '.js');
			echo "\xA".'</script>';
		}
		if ($headerfooter && $footer) {
			require $footerpath . $footer . $footerext;
		}
	}

	public function addJavascript($javascript)
	{
		$this->Javascripts[] = $javascript;
	}

	public function addExternalJavascript($javascript)
	{
		$this->externalJavascripts[] = $javascript;
	}
	public function addAngularJs($javascript)
	{
		$this->AngularJs[] = $javascript;
	}

	public function addPluginJs($javascript)
	{
		$this->PluginJs[] = $javascript;
	}

	public function addBowerJavascript($javascript)
	{
		$this->externalJavascripts[] = URL.'assets/bower/'.$javascript.'.js';
	}

	public function addStyle($Style)
	{
		$this->Styles[] = $Style;
	}

	public function addBowerStyle($Style)
	{
		$this->externalStyles[] = URL.'assets/bower/'.$Style.'.css';
	}

	public function getTitle() {
		$title = (isset($this->pageConfig['title']) && $this->pageConfig['title']) ? $this->pageConfig['title'] : 'Siscoed';
		return $title.' - Sistema de Contenidos Educativos';
	}

	public function getActiveSideBar()
	{
		return (isset($this->pageConfig['activeSideBar'])) ? $this->pageConfig['activeSideBar'] : array();
	}

	public function getBreadCrumbs()
	{
		return (isset($this->pageConfig['breadCrumbs'])) ? $this->pageConfig['breadCrumbs'] : array();
	}

	public function getShowSideBar()
	{
		return (isset($this->pageConfig['showSideBar'])) ? $this->pageConfig['showSideBar'] : true;
	}

	public function handleLogin()
	{
		$this->user = Auth::handleLogin();
	}
	
	/*
	 * useful for handling the navigation's active/non-active link
	 * ...
	 * TODO
	 */
	private function checkForActiveController($filename, $navigation_controller) {
		
		$splitted_filename = explode("/", $filename);
		$active_controller = $splitted_filename[0];
		
		if ($active_controller == $navigation_controller) {
			return true;
		} else {
			return false;
		}
	}
	
	private function checkForActiveAction($filename, $navigation_action) {
		
		$splitted_filename = explode("/", $filename);
		$active_action = $splitted_filename[1];
		
		if ($active_action == $navigation_action) {
			
			return true;
			
		} else {
			
			return false;
		}
		
	}	
	
	private function checkForActiveControllerAndAction($filename, $navigation_controller_and_action = "") {

		if(!$navigation_controller_and_action) {
			$navigation_controller_and_action = $filename;
			$arr = explode("=", $_SERVER["QUERY_STRING"]);
			$filename = $arr[1];
		}
		
		$splitted_filename = explode("/", $filename);
		$active_controller = $splitted_filename[0];
		if(!isset($splitted_filename[1])) { $splitted_filename[1] = "index"; }
		$active_action = $splitted_filename[1];
		$active_command = (isset($splitted_filename[2])) ? $splitted_filename[2] : "";
		
		$splitted_filename = explode("/", $navigation_controller_and_action);
		$navigation_controller = $splitted_filename[0];
		$navigation_action = (@$splitted_filename[1]) ? $splitted_filename[1] : 'index';
		$navigation_command = (isset($splitted_filename[2])) ? $splitted_filename[2] : "";

		if ($active_controller == $navigation_controller AND $active_action == $navigation_action AND $active_command == $navigation_command) {
			return true;
		} else {
			return false;
		}
		
	}

	private function checkForError($error) {
		if (is_array(@$this->errors)) {
			foreach ($this->errors as $key => $value) {
			  if (is_array($error)) {
				foreach ($error as $id => $val) {
				  if($val == $value[0]) {
					  return true;
				  }
				}
			  } else {
				if($value[0] == $error) {
					return true;
				}
			  }
			}
		}
		return false;
	}

	private function getActiveController($filename)
	{
		$splitted_filename = explode("/", $filename);
		return $active_controller = $splitted_filename[0];
	}

	private function getActiveAction($filename='')
	{
		if(!$filename) {
			$arr = explode("=", $_SERVER["QUERY_STRING"]);
			$filename = $arr[1];
		}
		$splitted_filename = explode("/", $filename);
		return (@$splitted_filename[1]) ? $active_action = @$splitted_filename[1] : "index";
	}

}