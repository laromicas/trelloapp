<?php

/**
 * handles the cookies stuff. creates cookies when no one exists, sets and
 * gets values, and closes the cookies properly (=logout). Those methods
 * are STATIC, which means you can run them with Session::get(XXX);
 * 
 * TODO: this is a static class/singleton. maybe this should be improved!
 */
class Cookie {

    /**
     * starts the cookie
     */
    public static function init() {
        
        // if no cookie exist, start the cookie
        if (session_id() == '') {
            session_start();
        }
    }

    /**
     * sets a specific value to a specific key of the cookie
     * @param mixed $key
     * @param mixed $value
     */
    public static function set($key, $value,$expire=0,$url=URL,$domain='',$secure='') {
        if($secure) {
            setcookie($key,$value,$expire,$url,$domain,$secure);
        } elseif($domain) {
            setcookie($key,$value,$expire,$url,$domain);
        } else {
            setcookie($key,$value,$expire,$url);
        }
    }

    /**
     * gets/returns the value of a specific key of the cookie
     * @param mixed $key Usually a string, right ?
     * @return mixed
     */
    public static function get($key) {
        if (isset($_COOKIE[$key])) {
            return $_COOKIE[$key];
        } else {
            return false;
        }
            
    }


    /**
     * deletes the cookies = logs the user out
     * moves the user to the index page of our app
     */
    public static function delete($key,$url=URL) {
        if (isset($_COOKIE[$key])) {
            setcookie ($key, "", time() - 3600,$url);
        }
    }

    /**
     * deletes the cookies = logs the user out
     * moves the user to the index page of our app
     */
    public static function destroy() {
        foreach ($_COOKIE as $key => $value) {
            Cookie::delete($key);
        }
    }

}