<?php

class Trello
{
	protected $TrelloApi;

	function __construct()
	{
		$this->user = Auth::handleLogin();
		$key = TRELLO_API_KEY;
		$token = Session::get('token');
		// var_dump($key, $token);
		$this->TrelloApi = new TrelloApi($key, $token);
	}

	public function getBoards() {
		return $this->TrelloApi->members->get('my/boards');
	}

	public function getBoard($board_id) {
		define('DEBUG_CURL',true);
		return $this->TrelloApi->boards->get($board_id);
	}

	public function getBoardCards($board_id) {
		return $this->TrelloApi->boards->get($board_id.'/cards');
	}

	public function getBoardLists($board_id) {
		return $this->TrelloApi->boards->get($board_id.'/lists');
	}

	public function getBoardMembers($board_id) {
		return $this->TrelloApi->boards->get($board_id.'/members');
	}

	public function getListCards($list_id) {
		return $this->TrelloApi->lists->get($list_id.'/cards');
	}

	public function getNotifications() {
		return $this->TrelloApi->members->get('my/notifications');
	}

	public function getMember($member_id) {
		return $this->TrelloApi->members->get($member_id);
	}

	public function getCard($card_id) {
		return $this->TrelloApi->cards->get($card_id);
	}

	public function getOrganizationMembers($organization_id) {
		return $this->TrelloApi->organizations->get($organization_id.'/members');
	}

	public function saveCard($card) {
		if(isset($card['idMembers']) && is_array($card['idMembers'])) {
			$card['idMembers'] = implode(',', $card['idMembers']);
		}
		if(isset($card['idLabels']) && is_array($card['idLabels'])) {
			$card['idLabels'] = implode(',', $card['idLabels']);
		}
		// var_dump($card);
		if($card['id'] == 'new') {
			return $this->TrelloApi->cards->post('',$card);
		} else {
			$id = $card['id'];
			unset($card['id']);
			return $this->TrelloApi->cards->put($id,$card);
		}
	}

	public function deleteCard($card) {
		if(is_string($card)) {
			$card_id = $card;
		} elseif(is_array($card)) {
			$card_id = $card['id'];
		} elseif(is_object($card)) {
			$card_id = $card->id;
		}
		$result = $this->TrelloApi->cards->del($card_id);
		return (isset($result)) ? true : false;
	}


}