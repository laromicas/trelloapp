TrelloApp.factory('userData', ['$http', function($http) { 
	return $http.get(fixurl+'data/user')
		.then(function(data) {
			toastr["success"]('Good to see you, '+data.data.name+'!');
			return data.data; 
		})
		// .error(function(err) { 
		// 	return err; 
		// });
}]);

TrelloApp.filter('phoneLink', function() {
	return function(input, icon) {
		input = input || '';
		input = input.trim();
		var out = '';
		if(input.length == 10) {
			out = '(';
			out += input.slice(0, 3);
			out += ') ';
			out += input.slice(3, 6);
			out += '-';
			out += input.slice(6);
			if(icon) {
				if(icon.length) {
					return '<a href="tel:'+input+'"><i class="'+icon+'"></i>'+out+'</a>';
				}
				return '<a href="tel:'+input+'"><i class="fa fa-phone"></i>'+out+'</a>';
			}
			return '<a href="tel:'+input+'">'+out+'</a>';
		} else {
			return input;
		}
	};
});

TrelloApp.filter('moment', function() {
	return function(input,format) {
		if(!input) {
			return '';
		}
		var date = moment(input);
		if(format) {
			newdate = date.format(format);
			return (newdate == 'Invalid date') ? '' : newdate;
		}
		return date.format();
	};
})

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs an AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform an OR.
 */
TrelloApp.filter('propsFilter', function() {
	return function(items, props) {
		var out = [];

		if (angular.isArray(items)) {
			var keys = Object.keys(props);

			items.forEach(function(item) {
				var itemMatches = false;

				for (var i = 0; i < keys.length; i++) {
					var prop = keys[i];
					var text = props[prop].toLowerCase();
					if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
						itemMatches = true;
						break;
					}
				}

				if (itemMatches) {
					out.push(item);
				}
			});
		} else {
			// Let the output be the input untouched
			out = items;
		}

		return out;
	};
});


// TrelloApp.service('notificationsService', [ function() {
// 	//Our data object
// 	var data = {
// 		clients: {}
// 	};

// 	var setClients = function (clients) {
// 		data.clients = clients;
// 	}

// 	var getClients = function () {
// 		return data.clients;
// 	}

// 	return {
// 		setClients: setClients,
// 		getClients: getClients
// 	};
// }]);

TrelloApp.service('getData', function($http) {
	//Our data object
	var boards = [];

	var getBoards = function () {
		return $http.get(fixurl+'data/boards')
			.then(function(data) {
				return data.data; 
			});
	}

	var getBoard = function (id) {
		return $http.get(fixurl+'data/board/'+id)
			.then(function(data) {
				return data.data; 
			});
	}

	var getCards = function (id) {
		return $http.get(fixurl+'data/cards/'+id)
			.then(function(data) {
				return data.data; 
			});
	}

	var getLists = function (id) {
		return $http.get(fixurl+'data/lists/'+id)
			.then(function(data) {
				return data.data; 
			});
	}

	var getNotifications = function (id) {
		return $http.get(fixurl+'data/notifications/')
			.then(function(data) {
				return data.data; 
			});
	}

	var getMembers = function (id) {
		return $http.get(fixurl+'data/members/'+id)
			.then(function(data) {
				return data.data; 
			});
	}

	var getAllMembers = function (id) {
		return $http.get(fixurl+'data/allmembers/'+id)
			.then(function(data) {
				return data.data; 
			});
	}

	return {
		getBoards: getBoards,
		getBoard: getBoard,
		getCards: getCards,
		getLists: getLists,
		getMembers: getMembers,
		getAllMembers: getAllMembers,
		getNotifications: getNotifications
	};
});