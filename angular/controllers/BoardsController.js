angular.module('TrelloApp').controller('BoardsController', function($rootScope, $scope, $http, $timeout, $uibModal, getData) {
	$scope.$on('$viewContentLoaded', function() {   
		// initialize core components
		App.initAjax();
	});

	// console.log($scope);

	$scope.boards = [];
	getData.getBoards().then(function (data) {
		$scope.boards = data;
	});

	$scope.activeBoard = {};


	$scope.setActive = function(event, index) {
		$('.page-sidebar ul li').removeClass('active');
		$('.page-sidebar ul li span.selected').remove();
		$(event.currentTarget).closest('li').addClass('active');
		$(event.currentTarget).append('<span class="selected"></span>');
		$scope.activeBoard = $scope.boards[index];
	}
});