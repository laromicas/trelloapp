angular.module('TrelloApp').controller('CardEditController', function($rootScope, $scope, $stateParams, $http, $timeout, $uibModal, selectedCard, selectedList, members, Upload) {
	$scope.$on('$viewContentLoaded', function() {
		// initialize core components
		App.initAjax();
	});

	console.log($scope);
	$scope.card = Object.assign({}, selectedCard);
	$scope.list = selectedList;
	if($scope.card.id == 'new') {
		$scope.card = {
				id: 'new',
				name: '',
				desc: '',
				pos: 'bottom',
				due: '',
				dueComplete: false,
				idList: $scope.list.id,
				idMembers: [],
				idLabels: []
			}
		$scope.date = { due: '' }
	} else {
		$scope.date = { due: moment($scope.card.due)._d }
	}

	$scope.members = members;

	$scope.popup = { opened: false };
	$scope.open = function () {
		$scope.popup.opened = true;
	}

	$scope.selectDate = function (date) {
		$scope.card.due = moment(date).toISOString();
	}

	$scope.toggleSelected = function(member_id) {
		if((index = $scope.card.idMembers.indexOf(member_id)) > -1) {
			$scope.card.idMembers.splice(index, 1);;
		} else {
			$scope.card.idMembers.push(member_id);
		}
	}

	$scope.close = function() {
		$rootScope.modalInstance.close();
	}

	$scope.save = function () {
		var savingcard = {
				id: $scope.card.id,
				name: $scope.card.name,
				desc: $scope.card.desc,
				pos: $scope.card.pos,
				due: $scope.card.due,
				dueComplete: $scope.card.dueComplete,
				idList: $scope.card.idList,
				idMembers: $scope.card.idMembers,
				idLabels: $scope.card.idLabels,
			}

		Upload.upload({
			url: fixurl+'data/save_card',
			data: {card: savingcard}
		}).then(function (data) {

			var message = 'Error Saving Card';
			var error = true;
			if(data.data.id) {
				message = 'Card Saved successfully';
				error = false;
				if($scope.card.id == 'new') {
					selectedCard = data.data;
					$scope.list.cards.push(selectedCard);
				} else {
					Object.assign(selectedCard, data.data);
				}
				selectedCard.members = [];
				for (var i = 0; i < $scope.members.length; i++) {
					if(selectedCard.idMembers.indexOf($scope.members[i].id) > -1) {
						selectedCard.members.push($scope.members[i]);
					}
				}
			}
			swal({
				title: (error) ? "Error" : "Success",
				text: message,
				type: (error) ? "error" : "success",
				confirmButtonClass: (error) ? "btn-error" : "btn-success",
				confirmButtonText: 'OK'
			});
			$scope.close();
		});
	}

});