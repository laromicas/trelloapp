angular.module('TrelloApp').controller('BoardViewController', function($rootScope, $scope, $stateParams, $http, $timeout, $uibModal, getData, Upload) {
	$scope.$on('$viewContentLoaded', function() {
		// initialize core components
		App.initAjax();
	});

	console.log($scope);

	$scope.board_id = $stateParams.id;
	$scope.board = $stateParams.board;
	$scope.lists = [];
	getData.getLists($scope.board_id).then(function (data) {
		$scope.lists = data;
	});

	if(!$scope.board) {
		getData.getBoard($scope.board_id).then(function (data) {
			$scope.board = data;
			getData.getAllMembers($scope.board.idOrganization).then(function (data) {
				$scope.members = data;
			});
		});
	} else {
		getData.getAllMembers($scope.board.idOrganization).then(function (data) {
			$scope.members = data;
		});
	}


	$scope.selectedCard = {};

	$scope.editCardModal = function(card, list) {
		$scope.selectedCard = card;
		$scope.selectedList = list;
		$rootScope.modalInstance = $uibModal.open({
			animation: true,
			templateUrl: fixurl+'views/angular/card_edit.html',
			resolve: {
				selectedCard: function () {
					return $scope.selectedCard;
				},
				selectedList: function () {
					return $scope.selectedList;
				},
				members: function () {
					return $scope.members;
				}
			},
			controller: 'CardEditController'
		});
		$rootScope.modalInstance.result.then(function (selectedItem) {
			$scope.selected = selectedItem;
		}, function () {
		});
	};


	$scope.deleteCard = function(list, cardindex) {
		card = list.cards[cardindex];
		if(confirm('Are you sure? this cannot be undone')) {
			Upload.upload({
				url: fixurl+'data/delete_card',
				data: {card_id: card.id}
			}).then(function (data) {

				var message = 'Error Deleting Card';
				var error = true;
				if(data.data == 'true') {
					message = 'Card Deleted successfully';
					error = false;
					list.cards.splice(cardindex,1);
				}
				swal({
					title: (error) ? "Error" : "Success",
					text: message,
					type: (error) ? "error" : "success",
					confirmButtonClass: (error) ? "btn-error" : "btn-success",
					confirmButtonText: 'OK'
				});
			});
		}
	}
});