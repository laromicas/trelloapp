<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" ng-app="TrelloApp">
	<!--<![endif]-->
	<!-- BEGIN HEAD -->

	<head>
		<meta charset="utf-8" />
		<title ng-bind="'Trello App | ' + $state.current.data.pageTitle"></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1" name="viewport" />
		<meta content="Preview page of Metronic Admin Theme #2 for statistics, charts, recent events and reports" name="description" />
		<meta content="" name="author" />
		<!-- BEGIN GLOBAL MANDATORY STYLES -->
		<!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
		<link rel="stylesheet" href="<?= URL ?>assets/css/font-awesome-core.css">
		<link rel="stylesheet" href="<?= URL ?>assets/css/font-awesome-solid.css">
		<link rel="stylesheet" href="<?= URL ?>assets/css/font-awesome-light.css">
		<link rel="stylesheet" href="<?= URL ?>assets/css/font-awesome-regular.css">
		<link rel="stylesheet" href="<?= URL ?>assets/css/font-awesome-brands.css">

		<link href="<?= URL ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= URL ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= URL ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= URL ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<!-- END GLOBAL MANDATORY STYLES -->
		<!-- BEGIN DYMANICLY LOADED CSS FILES(all plugin and page related styles must be loaded between GLOBAL and THEME css files ) -->
		<link id="ng_load_plugins_before" />
		<!-- END DYMANICLY LOADED CSS FILES -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<link href="<?= URL ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
		<!-- <link href="<?= URL ?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" /> -->
		<link href="<?= URL ?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= URL ?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
		<link href="<?= URL ?>assets/global/plugins/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
		<!-- END PAGE LEVEL PLUGINS -->
		<!-- BEGIN THEME GLOBAL STYLES -->
		<link href="<?= URL ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
		<link href="<?= URL ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
		<!-- END THEME GLOBAL STYLES -->
		<!-- BEGIN THEME LAYOUT STYLES -->
		<link href="<?= URL ?>assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
		<link href="<?= URL ?>assets/layouts/layout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
		<link href="<?= URL ?>assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />
		<link src="<?= URL ?>assets/global/plugins/bootstrap-toastr/toastr.min.css" type="text/javascript" />
		<link rel="stylesheet" href="<?= URL ?>assets/global/plugins/dropify/css/dropify.min.css">
		<link rel="stylesheet" href="<?= URL ?>assets/global/plugins/angularjs/plugins/ui-select/select.min.css">
		<link rel="stylesheet" href="<?= URL ?>assets/global/plugins/datatables/datatables.min.css">
		<link rel="stylesheet" href="<?= URL ?>assets/global/plugins/bootstrap-sweetalert/sweetalert.css">
		<link rel="stylesheet" href="<?= URL ?>assets/global/plugins/nvd3/nv.d3.css">
		
		<!-- <link rel="stylesheet" href="<?= URL ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css"> -->

		<!-- END THEME LAYOUT STYLES -->
		<link rel="shortcut icon" href="<?= URL ?>assets/img/icon.png" />
	</head>
	<!-- END HEAD -->

	<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid"
		not-ng-controller="AppController"> <!-- ng-class="{'page-sidebar-closed': settings.layout.pageSidebarClosed}" -->
		<!-- BEGIN HEADER -->
		<div class="page-header navbar navbar-fixed-top">
			<!-- BEGIN HEADER INNER -->
			<div class="page-header-inner ">
				<!-- BEGIN LOGO -->
				<div class="page-logo">
					<a href="<?= URL ?>">
						<img src="<?= URL ?>assets/img/logo.png" alt="logo" class="logo-default" style="width:130px;top: -10px;position: relative;" /> </a>
					<div class="menu-toggler sidebar-toggler">
						<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
					</div>
				</div>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
				<!-- END RESPONSIVE MENU TOGGLER -->
				<!-- BEGIN PAGE ACTIONS -->
				<!-- DOC: Remove "hide" class to enable the page header actions -->
				<div class="page-actions">
					<div class="btn-group">
					</div>
				</div>
				<!-- END PAGE ACTIONS -->
				<!-- BEGIN PAGE TOP -->
				<div class="page-top">
					<!-- BEGIN HEADER SEARCH BOX -->
					<!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
					<!-- <form class="search-form search-form-expanded" action="page_general_search_3.html" method="GET">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search..." name="query">
							<span class="input-group-btn">
								<a href="javascript:;" class="btn submit">
									<i class="icon-magnifier"></i>
								</a>
							</span>
						</div>
					</form> -->
					<!-- END HEADER SEARCH BOX -->
					<!-- BEGIN TOP NAVIGATION MENU -->
					<div class="top-menu" ng-controller="NotificationController">
						<ul class="nav navbar-nav pull-right">
							<!-- BEGIN NOTIFICATION DROPDOWN -->
							<!-- DOC: Apply "dropdown-dark" class below "dropdown-extended" to change the dropdown styte -->
							<!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
							<!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
							<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
								<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
									<i class="fal fa-bell"></i>
									<span class="badge badge-default"> {{ notifications.length }} </span>
								</a>
								<ul class="dropdown-menu" style="width: 350px; max-width: 350px;">
									<li class="external">
										<h3>
											<span class="bold">{{ notifications.length }} Alerts</span></h3>
										<a href="page_user_profile_1.html"></a>
									</li>
									<li>
										<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
											<li ng-repeat="notification in notifications">
												<a href="javascript:;">
													<span class="time">{{ notification.type }}</span>
													<span class="details">
														{{ ((notification.data.board) ? notification.data.board.name : (notification.data.card) ? notification.data.card.name : '') }}
													</span>
												</a>
											</li>
										</ul>
									</li>
								</ul>
							</li>
							<!-- END NOTIFICATION DROPDOWN -->
							<!-- BEGIN USER LOGIN DROPDOWN -->
							<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
							<li class="dropdown dropdown-user" ng-controller="UserController">
								<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
									<img alt="" class="img-circle" ng-src="{{ 'https://trello-avatars.s3.amazonaws.com/'+userData.avatarHash+'/30.png' }}" />
									<span class="username username-hide-on-mobile"> {{ userData.fullName }} </span>
									<i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu dropdown-menu-default">
									<li>
										<a href="#!/user/edit/{{ userData.user_id }}">
											<i class="fal fa-user"></i> My Profile </a>
									</li>
									<!-- <li>
										<a href="app_calendar.html">
											<i class="icon-calendar"></i> My Calendar </a>
									</li>
									<li>
										<a href="app_inbox.html">
											<i class="icon-envelope-open"></i> My Inbox
											<span class="badge badge-danger"> 3 </span>
										</a>
									</li>
									<li>
										<a href="app_todo_2.html">
											<i class="icon-rocket"></i> My Tasks
											<span class="badge badge-success"> 7 </span>
										</a>
									</li> -->
									<li class="divider"> </li>
									<li>
										<a href="<?= URL ?>login/logout">
											<i class="icon-key"></i> Log Out </a>
									</li>
								</ul>
							</li>
							<!-- END USER LOGIN DROPDOWN -->
							<!-- BEGIN QUICK SIDEBAR TOGGLER -->
							<!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
							<!-- <li class="dropdown dropdown-extended quick-sidebar-toggler">
								<span class="sr-only">Toggle Quick Sidebar</span>
								<i class="icon-logout"></i>
							</li> -->
							<!-- END QUICK SIDEBAR TOGGLER -->
						</ul>
					</div>
					<!-- END TOP NAVIGATION MENU -->
				</div>
				<!-- END PAGE TOP -->
			</div>
			<!-- END HEADER INNER -->
		</div>
		<!-- END HEADER -->
		<!-- BEGIN HEADER & CONTENT DIVIDER -->
		<div class="clearfix"> </div>
		<!-- END HEADER & CONTENT DIVIDER -->
		<!-- BEGIN CONTAINER -->
		<div class="page-container">
			<!-- BEGIN SIDEBAR -->
			<div class="page-sidebar-wrapper" ng-controller="BoardsController">
				<!-- END SIDEBAR -->
				<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
				<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
				<div class="page-sidebar navbar-collapse collapse">
					<ul class="page-sidebar-menu page-header-fixed page-sidebar-menu-hover-submenu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" ng-class="{'page-sidebar-menu-closed': settings.layout.pageSidebarClosed}">

					<li ng-repeat="board in boards">
						<a ui-sref="boardView({board: board, id: board.id })" ng-click="setActive($event, $index);" >
							<i class="fa fa-trello"></i>
							<span class="title">{{ board.name }}</span>
						</a>
					</li>
					</ul>
					<!-- END SIDEBAR MENU -->
				</div>
				<div class="page-sidebar navbar-collapse collapse">
				</div>
				<!-- END SIDEBAR -->
			</div>
			<!-- END SIDEBAR -->
			<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<!-- BEGIN CONTENT BODY -->
				<div class="page-content">
					<div ui-view class="fade-in-up"> </div>
					<!-- BEGIN PAGE HEADER-->
					<!-- END PAGE HEADER-->
				</div>
				<!-- END CONTENT BODY -->
			</div>
			<!-- END CONTENT -->
			<!-- BEGIN QUICK SIDEBAR -->
			<a href="javascript:;" class="page-quick-sidebar-toggler">
				<i class="icon-login"></i>
			</a>
			<div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
				<div class="page-quick-sidebar">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="javascript:;" data-target="#quick_sidebar_tab_1" data-toggle="tab"> Users
								<span class="badge badge-danger">2</span>
							</a>
						</li>
						<li>
							<a href="javascript:;" data-target="#quick_sidebar_tab_2" data-toggle="tab"> Alerts
								<span class="badge badge-success">7</span>
							</a>
						</li>
						<li class="dropdown">
							<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> More
								<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu pull-right">
								<li>
									<a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
										<i class="icon-bell"></i> Alerts </a>
								</li>
								<li>
									<a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
										<i class="icon-info"></i> Notifications </a>
								</li>
								<li>
									<a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
										<i class="icon-speech"></i> Activities </a>
								</li>
								<li class="divider"></li>
								<li>
									<a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
										<i class="icon-settings"></i> Settings </a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
			<!-- END QUICK SIDEBAR -->
		</div>
		<!-- END CONTAINER -->
		<!-- BEGIN FOOTER -->
		<div class="page-footer">
			<div class="page-footer-inner"> 2017 &copy; 
				<a target="_blank" href="http://mdagencia.com">Lácides Miranda</a> <!-- &nbsp;|&nbsp; -->
				<!-- <a href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Purchase Metronic!</a> -->
				<div class="scroll-to-top">
					<i class="icon-arrow-up"></i>
				</div>
			</div>
			<!-- END FOOTER -->
			<!-- BEGIN QUICK NAV -->
			<!-- <nav class="quick-nav">
				<a class="quick-nav-trigger" href="#0">
					<span aria-hidden="true"></span>
				</a>
				<ul>
					<li>
						<a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank" class="active">
							<span>Purchase Metronic</span>
							<i class="icon-basket"></i>
						</a>
					</li>
					<li>
						<a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/reviews/4021469?ref=keenthemes" target="_blank">
							<span>Customer Reviews</span>
							<i class="icon-users"></i>
						</a>
					</li>
					<li>
						<a href="http://keenthemes.com/showcast/" target="_blank">
							<span>Showcase</span>
							<i class="icon-user"></i>
						</a>
					</li>
					<li>
						<a href="http://keenthemes.com/metronic-theme/changelog/" target="_blank">
							<span>Changelog</span>
							<i class="icon-graph"></i>
						</a>
					</li>
				</ul>
				<span aria-hidden="true" class="quick-nav-bg"></span>
			</nav>
			<div class="quick-nav-overlay"></div> -->
			<!-- END QUICK NAV -->
			<!--[if lt IE 9]>
<script src="<?= URL ?>assets/global/plugins/respond.min.js"></script>
<script src="<?= URL ?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?= URL ?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
			<script src="<?= URL ?>config/fixurl.php" type="text/javascript"></script>
			<!-- BEGIN CORE PLUGINS -->
			<script src="<?= URL ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/lodash.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/nvd3/d3.min.js" type="text/javascript"></script>

			<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js" charset="utf-8"></script> -->
			<script src="<?= URL ?>assets/global/plugins/nvd3/nv.d3.min.js" type="text/javascript"></script>
			<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/nvd3/1.8.3/nv.d3.min.js"></script> -->

			<!-- END CORE PLUGINS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<script src="<?= URL ?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
			<!-- <script src="<?= URL ?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script> -->
			<!-- <script src="<?= URL ?>assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script> -->
			<script src="<?= URL ?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>

			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/chartjs/chart.min.js"></script>

			<script src="<?= URL ?>assets/global/plugins/ladda/spin.min.js"></script>
			<script src="<?= URL ?>assets/global/plugins/ladda/ladda.min.js"></script>
			<!-- <script src="<?= URL ?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script> -->

			<!-- END PAGE LEVEL PLUGINS -->
			<!-- BEGIN THEME GLOBAL SCRIPTS -->
			<script src="<?= URL ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
			<!-- END THEME GLOBAL SCRIPTS -->
			<!-- BEGIN PAGE LEVEL SCRIPTS -->
			<!-- <script src="<?= URL ?>assets/pages/scripts/dashboard.min.js" type="text/javascript"></script> -->
			<!-- END PAGE LEVEL SCRIPTS -->
			<!-- BEGIN THEME LAYOUT SCRIPTS -->
			<script src="<?= URL ?>assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
			<!-- END THEME LAYOUT SCRIPTS -->
			<!-- <script defer src="<?= URL ?>assets/js/packs/solid.js"></script>
			<script defer src="<?= URL ?>assets/js/packs/light.js"></script>
			<script defer src="<?= URL ?>assets/js/packs/regular.js"></script>
			<script defer src="<?= URL ?>assets/js/packs/brands.js"></script>
			<script defer src="<?= URL ?>assets/js/fontawesome.js"></script> -->
			<!-- BEGIN CORE ANGULARJS PLUGINS -->
			<script src="<?= URL ?>assets/global/plugins/angularjs/angular.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/angular-sanitize.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/angular-touch.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/plugins/angular-ui-router.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/plugins/ocLazyLoad.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/plugins/ui-bootstrap-tpls.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/angular-daterangepicker.min.js"></script>
			<script src="<?= URL ?>assets/global/plugins/jquery-easypiechart/angular.easypiechart.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/amcharts/amcharts/amChartsDirective.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/angular-countUp.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/chartjs/angular-chart.min.js"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/frapontillo.bootstrap-switch.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/dropify/js/dropify.min.js"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/plugins/ng-file-upload-all.min.js"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/plugins/ui-select/ui-select.js"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/angular-ui-mask.min.js"></script>

			<script src="<?= URL ?>assets/global/plugins/datatables/datatables.all.min.js"></script>
			<script src="<?= URL ?>assets/global/plugins/datatables/angular-datatables.min.js"></script>
			<script src="<?= URL ?>assets/global/plugins/datatables/angular-datatables-bootstrap.min.js"></script>
			<script src="<?= URL ?>assets/global/plugins/datatables/angular-datatables-buttons.min.js"></script>
			<script src="<?= URL ?>assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/angular-google-maps.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/angularjs/angular-simple-logger.js" type="text/javascript"></script>
			<!-- <script src="<?= URL ?>assets/global/plugins/morris/angular-morris.min.js" type="text/javascript"></script> -->
			<script src="<?= URL ?>assets/global/plugins/nvd3/angular-nvd3.min.js" type="text/javascript"></script>
			<script src="<?= URL ?>assets/global/plugins/ladda/angular-ladda.min.js"></script>
			<!-- <script src="https://rawgit.com/krispo/angular-nvd3/v1.0.7/dist/angular-nvd3.js"></script> -->
			<script src="<?= URL ?>assets/js/konami.js" type="text/javascript"></script>
		

			<!-- END CORE ANGULARJS PLUGINS -->
			<!-- ANGULARJS SCRIPTS -->
			<script src="<?= URL ?>angular/main.js" type="text/javascript"></script>
			<!-- <script src="<?= URL ?>angular/directives.js" type="text/javascript"></script> -->
			<script src="<?= URL ?>angular/services.js" type="text/javascript"></script>
			<script src="<?= URL ?>angular/controllers/BoardsController.js" type="text/javascript"></script>
			<!-- END ANGULARJS SCRIPTS -->


			<?php foreach ($this->externalJavascripts as $javascript) {
				echo '<script language="javascript" src="'.$javascript.'"></script>';
			} ?>
			<?php foreach ($this->Javascripts as $javascript) {
				Html::javascript($javascript);
			} ?>
			<?php foreach ($this->AngularJs as $javascript) {
				echo '<script language="javascript" src="'.URL.'assets/global/plugins/'.$javascript.'.js"></script>';
			} ?>

			<script>
				$(document).ready(function()
				{
					$('#clickmewow').click(function()
					{
						$('#radio1003').attr('checked', 'checked');
					});
				})
				Array.prototype.clean = function(deleteValue) {
					for (var i = 0; i < this.length; i++) {
						if (this[i] == deleteValue) {
							this.splice(i, 1);
							i--;
						}
					}
					return this;
				};
			</script>
	</body>

</html>