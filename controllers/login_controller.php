<?php

class Login_Controller extends Controller {

	public $specialLoad = "true";

	public function __call($name, $arguments) {
		$this->index($name);
	}

	function __construct() {
		parent::__construct();
	}

	function index() {
		if(!Session::get('URI')) {
			Session::set('URI',URL);
		}
		$this->view->render('login',array('headerfooter'=>false));
	}

	// Checks trello login and creates session user data
	function login() {
		$key = TRELLO_API_KEY;
		$token = $_POST['token'];
		$trello = new TrelloApi($key, $token);
		$login = $trello->authorize(array(
				'expiration' => '1hour',
				'scope' => array(
					'read' => true,
				),
				'name' => 'Trello Test App'
			));

		if(!$login) {
			Html::redirect_to(URL.'login');
		}


		$user = $trello->members->get('my');

		Session::set('user',$user);

		Session::set('user_logged_in',true);
		Session::set('token',$token);
		Html::redirect_to(URL);
	}

}