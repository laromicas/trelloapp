<?php

/**
* 
*/
class Data_Controller extends Controller
{
	
	function __construct() {
		parent::__construct();
		$this->view->user = Auth::handleLogin();
	}

	function user() {
		echo json_encode($this->view->user);
	}

	function boards() {
		$Trello = new Trello();
		$boards = $Trello->getBoards();
		echo json_encode($boards);
	}

	function board($board_id) {
		$Trello = new Trello();
		$board = $Trello->getBoard($board_id);
		echo json_encode($board);
	}

	function cards($list_id) {
		$Trello = new Trello();
		$cards = $Trello->getListCards($list_id);
		echo json_encode($cards);
	}

	function notifications() {

		// $json = '[{
		// 	"id": "59531f8674fc80b0b372880e",
		// 	"unread": true,
		// 	"type": "addedToBoard",
		// 	"date": "2017-06-28T03:16:22.628Z",
		// 	"data": {
		// 		"memberType": "normal",
		// 		"idMember": "5813b41ecaaa91c7a5dae8fe",
		// 		"board": {
		// 			"shortLink": "tBmYPSYe",
		// 			"name": "US National Parks",
		// 			"id": "560bf4298b3dda300c18d09c"
		// 		}
		// 	},
		// 	"idMemberCreator": "556c8537a1928ba745504dd8",
		// 	"memberCreator": {
		// 		"id": "556c8537a1928ba745504dd8",
		// 		"avatarHash": "0b48c7057767cca8f339109b27a064d7",
		// 		"fullName": "Matt Cowan",
		// 		"initials": "MC",
		// 		"username": "mattcowan"
		// 	}
		// }]';
		// echo $json;die();

		$Trello = new Trello();
		$notification = $Trello->getNotifications();
		echo json_encode($notification);
	}

	function lists($board_id) {
		$Trello = new Trello();
		$lists = $Trello->getBoardLists($board_id);

		$allmembers = array();
		// $newlists = array();
		foreach ($lists as $key => $list) {
			$list_id = $list->id;
			$cards = $Trello->getListCards($list_id);
			foreach ($cards as $keyCard => $card) {
				$members = array();
				foreach ($card->idMembers as $member) {
					if(!isset($allmembers[$member])) {
						$allmembers[$member] = $Trello->getMember($member);
					}
					$members[] = $allmembers[$member];

				}
				$card->members = $members;
			}
			$lists[$key]->cards = $cards;
		}
		echo json_encode($lists);
		// echo '<pre>';
		// print_r(json_decode(json_encode($lists), true));
	}

	function card($card_id) {
		$Trello = new Trello();
		$card = $Trello->getCard($card_id);
		echo json_encode($card);
		// echo '<pre>';
		// print_r(json_decode(json_encode($card), true));
	}

	function members($board_id) {
		$Trello = new Trello();
		$members = $Trello->getBoardMembers($board_id);
		echo json_encode($members);
	}

	function allmembers($organization_id) {
		$Trello = new Trello();
		$members = $Trello->getOrganizationMembers($organization_id);
		foreach ($members as $key => $member) {
			$data = $Trello->getMember($member->id);
			$members[$key] = $data;
		}
		echo json_encode($members);
	}

	function save_card() {
		$card = $_POST['card'];
		$Trello = new Trello();
		$result = $Trello->saveCard($card);
		echo json_encode($result);
	}

	function delete_card() {
		$card_id = $_POST['card_id'];
		// $card_id = 'ninguna';
		$Trello = new Trello();
		$result = $Trello->deleteCard($card_id);
		echo json_encode($result);
	}

}