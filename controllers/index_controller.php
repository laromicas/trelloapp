<?php

class Index_Controller extends Controller {
	function __construct() {
		parent::__construct();
		$this->view->handleLogin();
		$this->view->pageConfig['breadCrumbs'] = array();
		$this->view->pageConfig['sideMenu'] = array();
	}

	function index() {
		require ROOT_PATH.'config/sidemenu.php';
		$this->view->sideMenu = $GLOBALS['sidemenu'];


		$this->view->addPluginJs('isotope/isotope.pkgd.min');
		$this->view->addPluginJs('isotope/angular-isotope');


		$this->view->render('index',array('headerfooter'=>false));
	}

}