<?php

if(!class_exists('Error_Controller')) {
	class Error_Controller extends Controller {

		function __construct() {
			parent::__construct();
		}
		
		function index() {
			header("HTTP/1.0 404 Not Found");
			header("Status: 404 Not Found");

			// $this->view->Result = "Error 404. Oops. The page you are typing cannot be found. Please return to main page.";
			// echo $this->view->Result;
			$this->view->render('404',array('headerfooter'=>false));
			// $this->view->render('404');
		}
	}
}