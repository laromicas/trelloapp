<?php
/**
 * THIS IS THE CONFIGURATION FILE
 * 
 * For more info about constants please @see http://php.net/manual/en/function.define.php
 * If you want to know why we use "define" instead of "const" @see http://stackoverflow.com/q/2447791/1114320
 */

// date_default_timezone_set('America/Bogota');
date_default_timezone_set('America/New_York');

define('VCSSJS','v1');
define('FIX_GMT', " +2 hours");


define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_NAME', 'trello');
define('DB_USER', 'trello');
define('DB_PASS', 'trello');

define("HASH_COST_FACTOR", "10");


define("TRELLO_API_KEY","dcb96c6975f673f187a3c1ad4dd67277");

