<?php
$GLOBALS['statuses'] = array(
//	'E' => 'Temporal', //Temporal
	'A' => 'Activo', //Active
	'G' => 'Invitado', //Guest
	'O' => 'Mora', //Overdue
	'R' => 'Eliminado', //Removed
	'S' => 'Suspendido' //Suspended
);

$GLOBALS['positions'] = array(
		'President/CEO',
		'Recruiter',
		'On-Site',
		'Sales',
		'On-Site Recruiter',
		'Customer Service',
		'Branch Manager',
		'Customer Service/Operations/Sales',
		'Operations',
		'Other'
	);

$GLOBALS['staffpositions'] = array(
		'Heavy Lift',
		'Packing',
		'Cleaning',
		'Other'
	);

$GLOBALS['fillRateStatus'] = array(
		'A' => 'Active',
		'N' => 'No Order',
		'W' => 'Weather',
		'O' => 'Other'
	);
//type: V: Vip, A: Regional Operations Manager, O: Regional Onsite Manager, C: Onsite Manager
//access: S: Settings, M: Manage, C: Companies, U: Users, F: FillRates


$GLOBALS['colorTags'] = array('palegreen','darkorange','yellow','blue','green','purple','maroon','red','darkgray');

$GLOBALS['imagesExts'] = array("gif", "jpeg", "jpg", "png");
$GLOBALS['imageTypes'] = array("image/gif","image/jpeg","image/jpg","image/pjpeg","image/x-png","image/png");
