<?php
$GLOBALS["sidemenu"] = array(
	"Dashboard" => array(
		"start" => true,
		"link" => "#!/",
		"icon" => "icon-home",
		// "icon" => "fa fa-bar-chart",
		"access" => "VAOC"
	),
	"Users" => array(
		"icon" => "fal fa-user",
		"link" => "#!/users",
		"access" => "VA"
	),
	"Clients" => array(
		"link" => "#!/clients",
		"icon" => "fal fa-users",
		"access" => "VAOC"
	),
	"Dispatch"=> array(
		// "link" => "#!/dispatch",
		"link" => "javascript:;",
		"click" => "newDispatchModal()",
		"icon" => "fa fa-building-o",
		"access" => "VAOC"
	),
	"Reports" => array(
		// "link" => "#!/reports",
		"icon" => "fal fa-file-excel",
		"access" => "VAOC",
		"submenu" => array(
			"Daily Report" => array(
				"link" => "#!/reports/daily",
				"icon" => "fal fa-file-excel",
				"access" => "VAOC"
			),
			"Manpower Report" => array(
				"link" => "#!/reports/manpower",
				"icon" => "fal fa-file-excel",
				"access" => "VAOC"
			),
		)
	),
	"Settings" => array(
		// "link" => "#!/settings",
		"icon" => "fa fa-gears",
		"access" => "V",
		"submenu" => array(
			"Offices" => array(
				"link" => "#!/offices",
				"icon" => "fa fa-building-o",
				"access" => "VAOC"
			),
			"SMS Config" => array(
				"link" => "#!/sms",
				// "icon" => "fa fa-mobile",
				// "stackicon" => "fa fa-file-text",
				"icon" => "fa fa-file-text",
				"access" => "VAOC"
			)
		)
	)
);