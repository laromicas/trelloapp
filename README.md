# Trello Apps's README

This app is a Test App for [Trello](https://trello.com).

## BackEnd

### PHP

#### Framework

**@laromicas (Lacides Miranda)** based on *[Simple user-authentication solution - MVC FRAMEWORK](https://github.com/panique/php-login/)*

Used this MVC because of the fast deployment time.

> - **.htaccess** - Redirects all petitions to not found elements to index.php
> - **index.php** - Initiates global constraints and autoload functions and executes lib/Bootstrap.php
> - **config.php** - Global constants to configure the site, database and external connections
> - **constants.php** - Global Paths
> - **lib/Bootstrap.php** - The bootloader, examines url and execute the needed controller with the asked parameters
> - **lib/Controller.php** - Functions to extend the controllers
> - **liv/View.php** - Functions to render the frontend
> - **lib/Auth.php** - Some simple funcions to take care of login users and sessions
> - **lib/Cookie.php** - Wrapper to managing cookies
> - **lib/Session.php** - Wrapper to managing sessions
> - **lib/Html.php** - Shortcuts to headers and htmls
> - **lib/Helpers.php** - Helpers and direct functions
> - **lib/ORM.php** - [idiorm](https://j4mie.github.io/idiormandparis/) class -- a object oriented mapper and query builder
> - **lib/Model.php** - [paris](https://j4mie.github.io/idiormandparis/) class -- an active record manager** - uses idiorm
> - **lib/OAuthSimple.php** - [OAuthSimple](https://github.com/jrconlin/oauthsimple/) class -- a simple OAuth implementation
> - **controllers/** - All conections of the backend and the frontend are managed with controllers in this directory
> - **controllers/error_controller.php** - takes care of all server error requests
> - **controllers/index_controller.php** - root url
> - **models/** - All models refering to tables in the database
>
---

#### Scripts Specific to this App
> - **lib/TrelloApi.php** - All connections to the Trello Api are managed with this file
> - **lib/Trello.php** - A wrapper to TrelloApi
> 	- *functions:* 
> 		- *getBoards, getBoard, getBoardCards, getBoardLists, getBoardMembers, getListCards, getNotifications, getMember, getCard, getOrganizationMembers*: Read
> 		- *saveCard* Create and Put, determines if is a new card or is an update
> 		- *deleteCard* Delete
> - **controllers/login_controller.php** - takes care of login requests and trello validations
> - **controllers/data_controller.php** - prepares data to show to frontend
> 	- *functions:* 
> 		- *user, boards, board, cards, notifications, lists, card, members, allmembers*: Read
> 		- *save_card* Create and Put
> 		- *delete_card* Delete
>
---

### Database
Not used/needed but connection made with mysql

## FrontEnd

### HTML and Javascript Framework
All included inside *assets/* folder
> - [Metronic Template](http://keenthemes.com/preview/metronic/)
> - [jQuery](https://jquery.com/)
> - [AngularJS](https://angularjs.org/)
> - [FontAwesome](http://fontawesome.io/)
>
---

### Framework Files - php files
> - **views/404.php** - 404 Error Page
> - **views/login.php** - Login Page
> - **views/index.php** - App Page
>
---

### Views Files - html files
> - **views/angular/board.html** - Board with lists view
> - **views/angular/card_edit.html** - Card Form
>
---

### Control Files - javascript files
> - **angular/main.js** - angular definitions and main controller
> - **angular/services.js** - services to read data from backend
> - **angular/controllers/\*** - Controllers and inner functions to show, prepare, send and receive data
> - **angular/controllers/BoardsController.js** - List of boards (side menu)
> - **angular/controllers/BoardViewController.js** - Board, Lists and Card view
> - **angular/controllers/CardEditController.js** - Modal window to edit Card
>
---

